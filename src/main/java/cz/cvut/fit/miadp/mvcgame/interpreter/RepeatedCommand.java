/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.interpreter;

import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;

/**
 *
 * @author user
 */
public class RepeatedCommand implements ICommand{
    protected IGameModel receiver;
    private int repeating;
    private ICommand command;
    public RepeatedCommand(IGameModel receiver, int repeating, ICommand command){
        this.receiver = receiver;
        this.repeating = repeating;
        this.command = command;
    }

    @Override
    public void doExecute(final MacrocommandContext context, boolean createMemento) {
        if (createMemento) {
            context.setMementoForCommand(this.hashCode(), receiver.createMemento());
        }
        for(int i = 0; i < this.repeating; i++){
            this.command.doExecute(context, false);
        }
    }

    @Override
    public void unexecute(final MacrocommandContext context) {
        receiver.setMemento(context.getMementoForCommand(this.hashCode()));
    }
}
