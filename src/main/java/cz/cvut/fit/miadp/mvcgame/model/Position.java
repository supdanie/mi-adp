package cz.cvut.fit.miadp.mvcgame.model;

public class Position {

    private int dimX = 0;
    private int dimY = 0;

    public Position(Position pos) {
        this.dimX = pos.dimX;
        this.dimY = pos.dimY;
    }

    public Position(int posX, int posY) {
        this.dimX = posX;
        this.dimY = posY;
    }

    public int getX() {
        return dimX;
    }
    
    public int getY(){
        return dimY;
    }
    
    public void move(int dx, int dy) {
        this.dimX += dx;
        this.dimY += dy;
    }
}