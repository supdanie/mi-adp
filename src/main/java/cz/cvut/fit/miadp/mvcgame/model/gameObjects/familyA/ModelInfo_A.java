/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA;

import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsModelInfo;
import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;

/**
 *
 * @author user
 */
public class ModelInfo_A extends AbsModelInfo{
    private IGameModel model;
    
    public ModelInfo_A(IGameModel model){
        this.model = model;
    }
    
    public String getText(){
        String text = "Level: " + this.model.getLevel();
        text += ", Score: " + this.model.getScore();
        text += ", Shooting mode: " +this.model.getCannonShootingModeName();
        text += ", Force: " + this.model.getCannonForce();
        text += ", Angle: " + this.model.getCannonAngle();
        text += ", Gravity: " + this.model.getGravity();
        return text;
    }
}
