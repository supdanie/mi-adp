/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.factory;

import cz.cvut.fit.miadp.mvcgame.composite.CompositeObstacle;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.GameModelBuilder;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.RectangleWall;
import java.util.Random;

/**
 *
 * @author user
 */
public class LevelFactory {
    public static GameModel createGameModel(int level){
        if (level == 1) {
            return buildFirstLevel();
        } else if(level == 2) {
            return buildSecondLevel();
        } else {
            GameModelBuilder builder = new GameModelBuilder();
            CompositeObstacle component = new CompositeObstacle();
            for(int i = 0; i < 14; i++) {
                RectangleWall wall = new RectangleWall();
                wall.setPositionIfNull(new Position(120, 100 + i * 30));
                component.add(wall);
            }
            for(int i = 0; i < 14; i++) {
                RectangleWall wall = new RectangleWall();
                wall.setPositionIfNull(new Position(150 + i * 30, 130));
                component.add(wall);
            }
            builder.addObstacle(component);
            
            int count = 0;
            while(count < level * 5) {
                Random random = new Random();
                int posX = random.nextInt(MvcGameConfig.MAX_X - 200) + 100;
                int posY = random.nextInt(MvcGameConfig.MAX_X - 200) + 100;
                if (!component.isInObstacle(new Position(posX, posY))) {
                    builder.addEnemy(new Position(posX, posY));
                    count++;
                }
            }
            GameModel model = builder.build();
            model.setGravity(level == 3 ? 6 : 9);
            model.setLevel(level);
            return model;
        }
    }
    
    private static GameModel buildFirstLevel(){
        GameModelBuilder builder = new GameModelBuilder();

        for(int i = 0; i < 5; i++) {
            int x = 250 + i * 36;
            int y = 250 + i * 30; 
            builder.addEnemy(new Position(x, y));
        }
        
        return builder.build();
    }
    
    private static GameModel buildSecondLevel(){
        GameModelBuilder builder = new GameModelBuilder();
        
        for(int i = 0; i < 10; i++) {
            int x = 250 + i * 36;
            int y = 250 + i * 36; 
            builder.addEnemy(new Position(x, y));
        }
        
        CompositeObstacle component = new CompositeObstacle();
        for(int i = 0; i < 14; i++) {
            RectangleWall wall = new RectangleWall();
            wall.setPositionIfNull(new Position(120, 100 + i * 30));
            component.add(wall);
        }
        builder.setGravity(1);
        builder.setLevel(1);
        builder.addObstacle(component);
        GameModel model = builder.build();
        model.setGravity(3);
        model.setLevel(2);
        return model;
    }
    
}
