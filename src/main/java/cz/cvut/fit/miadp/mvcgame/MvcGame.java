package cz.cvut.fit.miadp.mvcgame;

import cz.cvut.fit.miadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.miadp.mvcgame.composite.CompositeObstacle;
import cz.cvut.fit.miadp.mvcgame.composite.ObstacleComponent;
import cz.cvut.fit.miadp.mvcgame.view.GameView;
import cz.cvut.fit.miadp.mvcgame.controller.GameController;
import java.util.List;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.factory.LevelFactory;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.GameModelBuilder;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.RectangleWall;
// in future, use Bridge to remove this dependency

public class MvcGame
{     
    private GameModel model;
    private GameView view;
    private GameController controller;
    private int actualLevel = 1;

    public void init()
    {      
        this.model = LevelFactory.createGameModel(this.actualLevel);
        this.view = new GameView(model);
        this.controller = this.view.makeController();
    }

    public void processPressedKeys(List<String> pressedKeysCodes)
    {
        for(String code : pressedKeysCodes)
        {
            controller.handleKeyPress(code);
        }
    }

    public void update()
    {
        this.model.timeTick();
    }

    public void render(IGameGraphics gr)
    {
        this.view.setGraphics(gr);
        this.view.render();
    }

    public String getWindowTitle()
    {
        return "The MI-ADP.16 MvcGame";
    }

    public int getWindowWidth()
    {
        return MvcGameConfig.MAX_X;
    }

    public int getWindowHeight()
    {
        return MvcGameConfig.MAX_Y;
    }
    
    public void increaseLevel(){
        this.actualLevel++;
        this.model = LevelFactory.createGameModel(this.actualLevel);
        this.view = new GameView(model);
        this.controller = this.view.makeController();
    }
    
    public boolean levelWon(){
        return this.model.won();
    }
}