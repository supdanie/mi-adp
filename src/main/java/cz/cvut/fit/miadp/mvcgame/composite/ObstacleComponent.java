/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.composite;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;
import java.util.List;

/**
 *
 * @author user
 */
public interface ObstacleComponent {
    boolean shouldRemoveAfterHit(List<AbsMissile> missiles);
    boolean isInObstacle(Position position);
    void accept(IVisitor visitor);
}
