/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.interpreter;

import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;
import java.util.List;

/**
 *
 * @author user
 */
public class CommandSequence implements ICommand {
    protected IGameModel receiver;
    private List<ICommand> commands;
    public CommandSequence(IGameModel receiver, List<ICommand> commands){
        this.commands = commands;
        this.receiver = receiver;
    }

    @Override
    public void doExecute(final MacrocommandContext context, boolean createMemento) {
        if(createMemento) {
            context.setMementoForCommand(this.hashCode(), receiver.createMemento());
        }
        for (ICommand command : this.commands){
            command.doExecute(context, false);
        }
    }

    @Override
    public void unexecute(final MacrocommandContext context) {
        receiver.setMemento(context.getMementoForCommand(this.hashCode()));
    }
}
