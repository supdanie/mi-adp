/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.interpreter;

import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;

/**
 *
 * @author user
 */
public interface ICommand {
    public void doExecute(final MacrocommandContext context, boolean createMemento);
    public void unexecute(final MacrocommandContext context);
}
