/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.view;

import cz.cvut.fit.miadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.miadp.mvcgame.composite.ObstacleComponent;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.controller.GameController;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsEnemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;
import cz.cvut.fit.miadp.mvcgame.observer.IObserver;
import cz.cvut.fit.miadp.mvcgame.proxy.GameModelProxy;
import cz.cvut.fit.miadp.mvcgame.visitor.GameRenderer;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

/**
 *
 * @author user
 */
public class GameView implements IObserver{
    private GameModel model;
    private IGameGraphics gr;
    
    private int updateCnt;
    private IVisitor renderer;
    
    public GameView(GameModel model){
        this.model = model;
        this.model.registerObserver(this);
        
        this.updateCnt = 1;
        this.renderer = new GameRenderer();
    }
    
    public void setGraphics(IGameGraphics gr){
        this.gr = gr;
        this.renderer.setGraphics(gr);
    }
    
    public GameController makeController() {
        return new GameController(new GameModelProxy(this.model));
    }

    @Override
    public void update() {
        this.updateCnt++;
    }
   

    public void render() {
        if (this.updateCnt > 0){
            this.gr.clearRect(0, 0, MvcGameConfig.MAX_X, MvcGameConfig.MAX_Y);
            for (GameObject obj : this.model.getGameObjects()) {
                obj.accept(this.renderer);
            }
            for (ObstacleComponent obstacle : this.model.getObstacles()){
                obstacle.accept(this.renderer);
            }
            this.updateCnt = 0;
        }
    }
}
