/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.model;

import cz.cvut.fit.miadp.mvcgame.composite.ObstacleComponent;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsEnemy;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author user
 */
public class GameModelBuilder {
    List<Position> enemies;
    List<ObstacleComponent> obstacles;
    private int level = 1;
    private int gravity = 1;
    
    public GameModelBuilder(){
        this.enemies = new ArrayList<>();
        this.obstacles = new ArrayList<>();
    }
    
    public void addEnemy(Position enemyPosition){
        this.enemies.add(enemyPosition);
    }
    
    public void addObstacle(ObstacleComponent obstacle){
        this.obstacles.add(obstacle);
    }
    
    public void setLevel(int level){
        this.level = level;
    }
    
    public void setGravity(int gravity){
        this.gravity = gravity;
    }
    
    public void clearEnemies(){
        this.enemies = new ArrayList<>();
    }
    
    public void clearObstacles(){
        this.obstacles = new ArrayList<>();
    }
    
    public GameModel build(){
        GameModel model = new GameModel(this.enemies, this.obstacles);
        model.setGravity(this.gravity);
        model.setLevel(this.level);
        return model;
    }
}
