/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.bridge;

import cz.cvut.fit.miadp.mvcgame.model.Position;

/**
 *
 * @author user
 */
public class GameGraphics implements IGameGraphics {

    protected IGameGraphicsImplementor implementor;
    
    public GameGraphics(IGameGraphicsImplementor implementor){
        this.implementor = implementor;
    }
    
    @Override
    public void drawImage(String path, Position pos) {
        this.implementor.drawImage(path, pos);
    }

    @Override
    public void drawText(String text, Position pos) {
        this.implementor.drawText(text, pos);
    }

    @Override
    public void drawRectangle(Position leftTop, Position rightBottom) {
        this.implementor.drawLine(
            leftTop,
            new Position(rightBottom.getX(), leftTop.getY())
        );
        this.implementor.drawLine(
            new Position(rightBottom.getX(), leftTop.getY()),
            rightBottom
        );
        this.implementor.drawLine(
            rightBottom,
            new Position(leftTop.getX(), rightBottom.getY())
        );
        this.implementor.drawLine(
            new Position(leftTop.getX(), rightBottom.getY()),
            leftTop
        );
    }

    @Override
    public void clearRect(int i, int i0, int MAX_X, int MAX_Y) {
       this.implementor.clearRect(i, i0, MAX_X, MAX_Y);
    }
    
}
