/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.bridge;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

/**
 *
 * @author user
 */
public class JavaFXGraphics implements IGameGraphicsImplementor {
    private GraphicsContext context;
    
    public JavaFXGraphics(GraphicsContext context){
        this.context = context;
    }
    
    @Override
    public void drawImage(String path, Position pos) {
        this.context.drawImage(new Image(path), pos.getX(), pos.getY());
    }

    @Override
    public void drawText(String text, Position pos) {
        this.context.fillText(text, pos.getX(), pos.getY());
    }

    @Override
    public void drawLine(Position startPos, Position endPos) {
        this.context.strokeLine(startPos.getX(), startPos.getY(), endPos.getX(), endPos.getY());
    }

    @Override
    public void clearRect(int i, int i0, int MAX_X, int MAX_Y) {
        this.context.clearRect(i, i0, MAX_X, MAX_Y);
    }
    
}
