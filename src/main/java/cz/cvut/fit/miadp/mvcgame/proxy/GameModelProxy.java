/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.proxy;

import cz.cvut.fit.miadp.mvcgame.command.AbstractCommand;
import cz.cvut.fit.miadp.mvcgame.composite.ObstacleComponent;
import cz.cvut.fit.miadp.mvcgame.interpreter.ICommand;
import java.util.List;

/**
 *
 * @author user
 */
public class GameModelProxy implements IGameModel {
    
    private IGameModel subject;
    
    public GameModelProxy(IGameModel subject){
        this.subject = subject;
    }

    @Override
    public void moveCannonUp() {
        this.subject.moveCannonUp();
    }

    @Override
    public void moveCannonDown() {
        this.subject.moveCannonDown();
    }

    @Override
    public void cannonShoot() {
        this.subject.cannonShoot();
    }

    @Override
    public void cannonToggleMode() {
        this.subject.cannonToggleMode();
    }

    @Override
    public void registerCommand(ICommand command) {
        this.subject.registerCommand(command);
    }

    @Override
    public Object createMemento() {
        return this.subject.createMemento();
    }

    @Override
    public void setMemento(Object memento) {
        this.subject.setMemento(memento);
    }

    @Override
    public void undoLastCommand() {
        this.subject.undoLastCommand();
    }

    @Override
    public void cannonChangeAngle(boolean increase) {
        this.subject.cannonChangeAngle(increase);
    }

    @Override
    public void cannonChangeForce(boolean increase) {
        this.subject.cannonChangeForce(increase);
    }

    @Override
    public void setGravity(int gravity) {
        this.subject.setGravity(gravity);
    }

    @Override
    public int getGravity() {
        return this.subject.getGravity();
    }

    @Override
    public String getCannonShootingModeName() {
        return this.subject.getCannonShootingModeName();
    }

    @Override
    public int getScore() {
        return this.subject.getScore();
    }

    @Override
    public int getCannonAngle() {
        return this.subject.getCannonAngle();
    }

    @Override
    public int getCannonForce() {
        return this.subject.getCannonForce();
    }

    @Override
    public List<ObstacleComponent> getObstacles() {
        return this.subject.getObstacles();
    }

    @Override
    public int getLevel() {
        return this.subject.getLevel();
    }
    
}
