/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.composite.ObstacleComponent;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;
import java.util.List;

/**
 *
 * @author user
 */
public class RectangleWall extends GameObject implements ObstacleComponent{

    @Override
    public void accept(IVisitor visitor) {
        visitor.visitWall(this);
    }

    @Override
    public boolean shouldRemoveAfterHit(List<AbsMissile> missiles) {
        for(AbsMissile m : missiles){
            if (m.getX() >= this.getX() 
                    && m.getX() <= this.getX() + MvcGameConfig.OBSTACLE_SIZE 
                    && m.getY() >= this.getY() 
                    && m.getY() <= this.getY() + MvcGameConfig.OBSTACLE_SIZE) {
                
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean isInObstacle(Position position) {
        if (position.getX() >= this.getX() 
                    && position.getX() <= this.getX() + MvcGameConfig.OBSTACLE_SIZE 
                    && position.getY() >= this.getY() 
                    && position.getY() <= this.getY() + MvcGameConfig.OBSTACLE_SIZE) {
                
                return true;
        }
        return false;
    }
    
}
