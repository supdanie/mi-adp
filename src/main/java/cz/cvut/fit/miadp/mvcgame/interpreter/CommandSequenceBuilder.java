/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.interpreter;

import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;
import java.util.ArrayList;
/**
 *
 * @author user
 */
public class CommandSequenceBuilder {
    private ArrayList<ICommand> commands;
    private final IGameModel model;
    
    public CommandSequenceBuilder(IGameModel model){;
        this.commands = new ArrayList<>();
        this.model = model;
    }
   
    public void addCommand(ICommand command){
        this.commands.add(command);
    }
    
    public void clearCommands(){
        this.commands = new ArrayList<ICommand>();
    }
    
    public CommandSequence build(){
        return new CommandSequence(this.model, this.commands);
    }   
}
