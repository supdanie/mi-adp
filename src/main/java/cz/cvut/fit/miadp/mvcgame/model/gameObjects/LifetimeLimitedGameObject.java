/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;
import java.util.Date;

/**
 *
 * @author user
 */
public abstract class LifetimeLimitedGameObject extends GameObject{
    protected Date bornAt = new Date();
    
    
    public long getAge(){
        return (new Date().getTime() - this.bornAt.getTime());
    }

}
