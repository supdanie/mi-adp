/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.observer;

/**
 *
 * @author user
 */
public interface IObservable {
    void registerObserver(IObserver obs);
    void unregisterObserver(IObserver obs);
    void notifyObservers();
}
