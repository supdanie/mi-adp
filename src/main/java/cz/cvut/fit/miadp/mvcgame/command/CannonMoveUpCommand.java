/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;

/**
 *
 * @author user
 */
public class CannonMoveUpCommand extends AbstractCommand {
    
    public CannonMoveUpCommand(IGameModel receiver){
        super(receiver);
    }

    @Override
    public void execute() {
        this.receiver.moveCannonUp();
    }

    @Override
    public boolean equals(Object obj) {
        return obj.getClass() == this.getClass();
    }
    
    
    
}
