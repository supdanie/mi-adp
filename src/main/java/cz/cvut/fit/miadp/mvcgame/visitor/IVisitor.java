/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.visitor;

import cz.cvut.fit.miadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCollision;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsEnemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsModelInfo;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.RectangleWall;

/**
 *
 * @author user
 */
public interface IVisitor {
    void visitCannon(AbsCannon go);
    void visitCollision(AbsCollision go);
    void visitEnemy(AbsEnemy go);
    void visitMissile(AbsMissile go);
    void visitModelInfo(AbsModelInfo go);
    void visitWall(RectangleWall go);
    
    void setGraphics(IGameGraphics gr);
}
