/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;

/**
 *
 * @author user
 */
public class RealisticMovingStrategy implements IMovingStrategy {
    
    public int gravity;
    
    public void setGravity(int gravity){
        this.gravity = gravity;
    }
    
    @Override
    public void updatePosition(AbsMissile missile) {
        
        if(missile.getX() > MvcGameConfig.MAX_X) {
            return;
        }
        
        int x1 = missile.getX() - MvcGameConfig.CANNON_INIT_X;
        int x2 = x1 + missile.getInitialForce();
        int angle = missile.getInitialAngle();
        int force = missile.getInitialForce();
        double gravity = 0.1 * this.gravity;
        int difference = (int) (Math.tan(Math.toRadians(angle)) * force) - (int)(gravity * x2 * x2 / (force * force)) + (int)(gravity * x1 * x1 / (force * force));
        missile.move(missile.getInitialForce(), -difference);
       
    }
    
}
