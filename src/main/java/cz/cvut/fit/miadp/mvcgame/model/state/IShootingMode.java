/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.model.state;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;

/**
 *
 * @author user
 */
public interface IShootingMode {
    public String getName();
    public void shoot(AbsCannon cannon);
    public void nextMode(AbsCannon cannon);
}
