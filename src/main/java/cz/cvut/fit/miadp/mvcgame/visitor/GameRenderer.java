/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.visitor;

import cz.cvut.fit.miadp.mvcgame.bridge.IGameGraphics;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCollision;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsEnemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsModelInfo;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.RectangleWall;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;

/**
 *
 * @author user
 */
public class GameRenderer implements IVisitor {   
    
    private IGameGraphics gr;
    
    public GameRenderer(){
    }
    
    @Override
    public void setGraphics(IGameGraphics gr) {
        this.gr = gr;
    }

    @Override
    public void visitCannon(AbsCannon go) {
        this.drawImage("images/cannon.png", new Position(go.getX(), go.getY()));
    }

    @Override
    public void visitCollision(AbsCollision go) {
        if (go.getAge() < 1000) {
            this.drawImage("images/collision.png", new Position(go.getX(), go.getY()));
        }
    }

    @Override
    public void visitEnemy(AbsEnemy go) {
        this.drawImage("images/enemy1.png", new Position(go.getX(), go.getY()));
    }

    @Override
    public void visitMissile(AbsMissile go) {
        this.drawImage("images/missile.png", new Position(go.getX(), go.getY()));
    }

    @Override
    public void visitModelInfo(AbsModelInfo go) {
        this.gr.drawText(go.getText(), new Position(MvcGameConfig.INFO_X, MvcGameConfig.INFO_Y));
    }
    
    private void drawImage(String path, Position imgPos){
        if (this.gr == null) return;
        this.gr.drawImage(path, imgPos);
    }

    @Override
    public void visitWall(RectangleWall go) {
        this.drawImage("images/obstacle.png", new Position(go.getX(), go.getY()));
    }
}
