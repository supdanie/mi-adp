/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;

/**
 *
 * @author user
 */
public class CannonChangeAngleCommand extends AbstractCommand {
    private boolean increase;
    public CannonChangeAngleCommand(IGameModel receiver, boolean increase){
        super(receiver);
        this.increase = increase;
    }

    @Override
    public void execute() {
        this.receiver.cannonChangeAngle(increase);
    }
    
     @Override
    public boolean equals(Object obj) {
        if (obj.getClass() == this.getClass()){
            return this.increase == ((CannonChangeAngleCommand) obj).increase;
        }
        return false;
    }
    
    
}
