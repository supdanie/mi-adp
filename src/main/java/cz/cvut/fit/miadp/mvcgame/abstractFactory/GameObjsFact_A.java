package cz.cvut.fit.miadp.mvcgame.abstractFactory;

import cz.cvut.fit.miadp.mvcgame.composite.ObstacleComponent;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCollision;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsEnemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsModelInfo;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.RectangleWall;
import cz.cvut.fit.miadp.mvcgame.composite.CompositeObstacle;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.Cannon_A;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.Collision_A;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.Enemy_A;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.Missile_A;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.ModelInfo_A;
import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;
import cz.cvut.fit.miadp.mvcgame.strategy.IMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.RealisticMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMovingStrategy;
import java.util.Random;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author user
 */
public class GameObjsFact_A implements IGameObjsFact {

    private IGameModel model;
    
    public GameObjsFact_A(IGameModel model){
        this.model = model;
    }
    
    @Override
    public AbsCannon createCannon() {
        Cannon_A c = new Cannon_A(this);
        Position cannonPos = new Position(MvcGameConfig.CANNON_INIT_X, MvcGameConfig.CANNON_INIT_Y);
        c.setPositionIfNull(cannonPos);
        return c;
    }

    @Override
    public AbsEnemy createEnemy() {
        return new Enemy_A();
    }

    @Override
    public AbsCollision createCollision() {
        return new Collision_A();
    }

    @Override
    public AbsMissile createMissile() {
        IMovingStrategy strategy = new RealisticMovingStrategy();
        strategy.setGravity(this.model.getGravity());
        return new Missile_A(strategy);
    }

    @Override
    public AbsModelInfo createModelInfo() {
        return new ModelInfo_A(this.model);
    }

    @Override
    public ObstacleComponent createSimpleObstacle(int startX, int startY, boolean vertical) {
        CompositeObstacle component = new CompositeObstacle();
        for (int i = 0; i < 5; i++) {
            RectangleWall wall = new RectangleWall();
            int actualX = vertical ? startX : startX + i * MvcGameConfig.OBSTACLE_SIZE;
            int actualY = vertical ? startY + i * MvcGameConfig.OBSTACLE_SIZE : startX;
            wall.setPositionIfNull(new Position(actualX, actualY));
            component.add(wall);
        }
        return component;
    }
    
}
