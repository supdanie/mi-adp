/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;

/**
 *
 * @author user
 */
public class SimpleMovingStrategy implements IMovingStrategy {
    
    public int gravity;
    
    public void setGravity(int gravity){
        this.gravity = gravity;
    }

    @Override
    public void updatePosition(AbsMissile missile) {
        double exactStepY = Math.tan(Math.toRadians(missile.getInitialAngle())) * missile.getInitialForce();
        int stepY = (int) (Math.round(exactStepY));
        missile.move(missile.getInitialForce(), -stepY);
    }
    
}
