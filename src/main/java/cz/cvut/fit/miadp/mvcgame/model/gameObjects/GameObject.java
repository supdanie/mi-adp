/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

/**
 *
 * @author user
 */
public abstract class GameObject {
    
    protected Position position;
    
    public GameObject() {
    }
    
    public GameObject(Position position) {
        this.position = position;
    }
    
    public int getX() {
        return this.position.getX();
    }
    
    public int getY() {
        return this.position.getY();
    }
    
    public void setPositionIfNull(Position position) {
        if (this.position == null) {
            this.position = position;
        }
    }
    
    
    public void move(int dx, int dy){
        this.position.move(dx, dy);
    }
    
    public abstract void accept(IVisitor visitor);
}
