/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.abstractFactory;

import cz.cvut.fit.miadp.mvcgame.composite.ObstacleComponent;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCollision;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsEnemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsModelInfo;

/**
 *
 * @author user
 */
public interface IGameObjsFact {
    AbsCannon createCannon();
    AbsEnemy createEnemy();
    AbsCollision createCollision();
    AbsMissile createMissile();
    AbsModelInfo createModelInfo();
    ObstacleComponent createSimpleObstacle(int startX, int startY, boolean vertical);
}
