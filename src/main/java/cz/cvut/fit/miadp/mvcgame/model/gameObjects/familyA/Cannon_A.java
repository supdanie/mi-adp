/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.IGameObjsFact;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.model.state.DoubleShootingMode;
import cz.cvut.fit.miadp.mvcgame.model.state.IShootingMode;
import cz.cvut.fit.miadp.mvcgame.model.state.SimpleShootingMode;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author user
 */
public class Cannon_A extends AbsCannon {

    private IGameObjsFact goFactory;
    List<AbsMissile> batch = new ArrayList<AbsMissile>();
    private IShootingMode mode;
    
    private static final IShootingMode SINGLE_MODE = new SimpleShootingMode();
    private static final IShootingMode DOUBLE_MODE = new DoubleShootingMode();
    
    public Cannon_A(IGameObjsFact goFactory) {
        this.goFactory = goFactory;
        this.useSimpleMode();
    }
    
    @Override
    public void moveUp() {
        if (this.getX() > 0){
            this.move(0, -1 * MvcGameConfig.MOVE_STEP);
        }
    }

    @Override
    public void moveDown() {
        if (this.getX() < MvcGameConfig.MAX_Y){
            this.move(0, MvcGameConfig.MOVE_STEP);
        }
    }

    @Override
    public List<AbsMissile> shoot() {
        this.batch.clear();
        this.mode.shoot(this);
        return this.batch;
    }
    
    public void toggle() {
        this.mode.nextMode(this);
    }

    @Override
    public AbsMissile primitiveShoot() {
        AbsMissile m = this.goFactory.createMissile();
        m.setPositionIfNull(new Position(this.position.getX(), this.position.getY()));
        m.setInitialAngle(this.angle);
        m.setInitialForce(this.force);
        this.batch.add(m);
        return m;
    }

    @Override
    public void useSimpleMode() {
        this.mode = SINGLE_MODE;
    }

    @Override
    public void useDoubleMode() {
        this.mode = DOUBLE_MODE;
    }

    @Override
    public IShootingMode actualMode() {
        return this.mode;
    }
    
}
