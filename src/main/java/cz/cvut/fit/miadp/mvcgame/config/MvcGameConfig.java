package cz.cvut.fit.miadp.mvcgame.config;

public class MvcGameConfig 
{
    public static final int MAX_X = 1280;
    public static final int MAX_Y = 720;
    public static final String UP = "UP";
    public static final String DOWN = "DOWN";
    public static final int STEP_X = 10;
    public static final int STEP_Y = 10;
    public static final int CANNON_INIT_X = 10;
    public static final int CANNON_INIT_Y = 360;
    
    public static final int INITIAL_FORCE = 16;
    public static final int MIN_FORCE = 1;
    public static final int MAX_FORCE = 100;
    
    public static final int MIN_ANGLE = -80;
    public static final int MAX_ANGLE = 80;
    
    public static final int INFO_X = 10;
    public static final int INFO_Y = 10;
    
    public static final int MOVE_STEP = 10;
    
    public static final int MAX_MISS_OF_ENEMY = 25;
    
    public static final int DEFAULT_GRAVITY = 5;

    public static final int OBSTACLE_SIZE = 30;
}