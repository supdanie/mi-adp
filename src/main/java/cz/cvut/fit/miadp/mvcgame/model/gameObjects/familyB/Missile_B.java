/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyB;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.strategy.IMovingStrategy;

/**
 *
 * @author user
 */
public class Missile_B extends AbsMissile {
    IMovingStrategy movementStrategy;
    
    public Missile_B(IMovingStrategy movementStrategy){
        this.movementStrategy = movementStrategy;
    }
    
    @Override
    public void move() {
        this.movementStrategy.updatePosition(this);
    }
}
