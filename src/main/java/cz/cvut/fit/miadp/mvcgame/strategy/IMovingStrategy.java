/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.strategy;

import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;

/**
 *
 * @author user
 */
public interface IMovingStrategy {
    public void setGravity(int gravity);
    public void updatePosition(AbsMissile missile);
}
