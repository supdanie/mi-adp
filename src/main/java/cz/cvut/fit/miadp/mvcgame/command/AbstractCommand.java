/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.command;

import cz.cvut.fit.miadp.mvcgame.interpreter.ICommand;
import cz.cvut.fit.miadp.mvcgame.interpreter.MacrocommandContext;
import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;

/**
 *
 * @author user
 */
public abstract class AbstractCommand implements ICommand {
    protected IGameModel receiver;
    protected Object memento;
    
    public AbstractCommand(IGameModel receiver) {
        this.receiver = receiver;
    }
    
    @Override
    public void doExecute(final MacrocommandContext context, boolean createMemento){
        if (createMemento){
            this.memento = this.receiver.createMemento();
        }
        this.execute();
    }
    
    @Override
    public void unexecute(final MacrocommandContext context){
        if (this.memento != null) {
            this.receiver.setMemento(this.memento); 
        }
    }
    
    public abstract void execute();
}
