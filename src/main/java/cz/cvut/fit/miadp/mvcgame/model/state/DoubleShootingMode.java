/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.model.state;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;

/**
 *
 * @author user
 */
public class DoubleShootingMode implements IShootingMode {

    @Override
    public String getName() {
        return "Double";
    }

    @Override
    public void shoot(AbsCannon cannon) {
        AbsMissile m;
        m = cannon.primitiveShoot();
        m.move(0, MvcGameConfig.MOVE_STEP);
        m = cannon.primitiveShoot();
        m.move(0, -1 * MvcGameConfig.MOVE_STEP);
    }

    @Override
    public void nextMode(AbsCannon cannon) {
        cannon.useSimpleMode();
    }
    
}
