/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.state.IShootingMode;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;
import java.util.List;

/**
 *
 * @author user
 */
public abstract class AbsCannon extends GameObject {
    
    protected int angle = 0;
    protected int force = 16;
    public void accept(IVisitor visitor){
        visitor.visitCannon(this);
    }
    
    
    public abstract void moveDown();
    public abstract void moveUp();
    
    public int getAngle(){
        return this.angle;
    }
    
    public void increaseAngle(){
        if (this.angle < MvcGameConfig.MAX_ANGLE){
            this.angle += 1;
        }
    }
    
    public void decreaseAngle(){
        if (this.angle > MvcGameConfig.MIN_ANGLE){
            this.angle -= 1;
        }
    }
    
    public int getForce(){
        return this.force;
    }
    
    
    public void increaseForce(){
        if (this.force < MvcGameConfig.MAX_FORCE){
            this.force += 1;
        }
    }
    
    public void decreaseForce(){
        if (this.force > MvcGameConfig.MIN_FORCE){
            this.force -= 1;
        }
    }
    
    public void setForce(int force){
        this.force = force;
    }
    
    public void setAngle(int angle){
        this.angle = angle;
    }
    
    public abstract List<AbsMissile> shoot();

    public abstract AbsMissile primitiveShoot();

    public abstract void useSimpleMode();

    public abstract void useDoubleMode();
    
    public abstract IShootingMode actualMode();
    
    public abstract void toggle();
}
