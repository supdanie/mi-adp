/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

/**
 *
 * @author user
 */
public abstract class AbsModelInfo extends GameObject {
    public void accept(IVisitor visitor){
        visitor.visitModelInfo(this);
    }
    
    public abstract String getText();
}
