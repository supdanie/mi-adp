/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.composite;

import cz.cvut.fit.miadp.mvcgame.composite.ObstacleComponent;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author user
 */
public class CompositeObstacle implements ObstacleComponent {

    private List<ObstacleComponent> components = new ArrayList<ObstacleComponent>();
    
    public void add(ObstacleComponent component) {
        components.add(component);
    }
    
    public void remove(ObstacleComponent component){
        components.remove(component);
    }
    
    @Override
    public void accept(IVisitor visitor) {
        for (ObstacleComponent component : this.components){
            component.accept(visitor);
        }
    }

    @Override
    public boolean shouldRemoveAfterHit(List<AbsMissile> missiles) {
        boolean shouldRemove = false;
        for(ObstacleComponent component : this.components) {
            boolean toRemove = component.shouldRemoveAfterHit(missiles);
            if (!toRemove) {
                shouldRemove = false;
            } else {
                this.remove(component);
            }
        }
        return shouldRemove;
    }

    @Override
    public boolean isInObstacle(Position position) {
        for(ObstacleComponent component : this.components) {
            if (component.isInObstacle(position)){
                return true;
            }
        }
        return false;
    }
    
}
