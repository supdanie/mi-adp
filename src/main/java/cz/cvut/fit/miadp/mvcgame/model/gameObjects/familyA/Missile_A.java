/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA;

import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.strategy.IMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMovingStrategy;

/**
 *
 * @author user
 */
public class Missile_A extends AbsMissile {

    IMovingStrategy movementStrategy;
    
    public Missile_A(IMovingStrategy movementStrategy){
        this.movementStrategy = movementStrategy;
    }
    
    @Override
    public void move() {
        this.movementStrategy.updatePosition(this);
    }
    
}
