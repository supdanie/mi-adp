/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.model.gameObjects;

import cz.cvut.fit.miadp.mvcgame.visitor.IVisitor;

/**
 *
 * @author user
 */
public abstract class AbsMissile extends GameObject {
    
    protected int initialForce;
    protected int initialAngle;
    
    public void accept(IVisitor visitor){
        visitor.visitMissile(this);
    }

    public int getInitialAngle() {
        return initialAngle;
    }

    public int getInitialForce() {
        return initialForce;
    }

    public void setInitialAngle(int initialAngle) {
        this.initialAngle = initialAngle;
    }

    public void setInitialForce(int initialForce) {
        this.initialForce = initialForce;
    }
    
    public abstract void move();
}
