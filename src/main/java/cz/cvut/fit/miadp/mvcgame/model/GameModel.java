/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.model;

import cz.cvut.fit.miadp.mvcgame.abstractFactory.GameObjsFact_A;
import cz.cvut.fit.miadp.mvcgame.abstractFactory.IGameObjsFact;
import cz.cvut.fit.miadp.mvcgame.command.AbstractCommand;
import cz.cvut.fit.miadp.mvcgame.composite.ObstacleComponent;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.interpreter.ICommand;
import cz.cvut.fit.miadp.mvcgame.interpreter.MacrocommandContext;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCollision;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsEnemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsModelInfo;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.GameObject;
import cz.cvut.fit.miadp.mvcgame.observer.IObservable;
import cz.cvut.fit.miadp.mvcgame.observer.IObserver;
import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;
import cz.cvut.fit.miadp.mvcgame.view.GameView;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Queue;
import java.util.Random;
import java.util.Stack;
import java.util.concurrent.LinkedBlockingQueue;

/**
 *
 * @author user
 */
public class GameModel implements IObservable, IGameModel {
    
    private int score = 0;
    private int level = 1;
    private int gravity = MvcGameConfig.DEFAULT_GRAVITY;
    
    private Queue<ICommand> unexecutedCommands = new LinkedBlockingQueue<ICommand>();
    private Stack<ICommand> executedCommands = new Stack<ICommand>();
    
    private AbsCannon cannon;
    private List<AbsEnemy> enemies;
    private List<AbsMissile> missiles;
    private List<AbsCollision> collisions;
    private List<ObstacleComponent> obstacles;
    private AbsModelInfo modelInfo;
    private MacrocommandContext context;
    
    
    private List<IObserver> myObservers;
    private IGameObjsFact goFact;
    
    public GameModel(List<Position> enemies, List<ObstacleComponent> obstacles) {
        this.myObservers = new ArrayList<IObserver>();
        this.goFact = new GameObjsFact_A(this);
        
        this.cannon = goFact.createCannon();
        this.modelInfo = goFact.createModelInfo();
        
        this.missiles = new ArrayList<AbsMissile>();
        
        this.collisions = new ArrayList<AbsCollision>();
        this.context = new MacrocommandContext();
        
        this.enemies = new ArrayList<>();
        for(Position enemyPosition : enemies){
            AbsEnemy enemy = this.goFact.createEnemy();
            enemy.setPositionIfNull(enemyPosition);
            this.enemies.add(enemy);
        }
        this.obstacles = obstacles;
    }
    
    public void setLevel(int level) {
        this.level = level;
    }
    
    public void setGravity(int gravity){
        this.gravity = gravity;
    }
    
    public int getGravity(){
        return this.gravity;
    }

    @Override
    public void moveCannonDown() {
       this.cannon.moveDown();
       this.notifyObservers();
    }

    @Override
    public void moveCannonUp() {
        this.cannon.moveUp();
        this.notifyObservers();
    }
    
    public List<GameObject> getGameObjects() {
        List<GameObject> objs = new ArrayList<GameObject>();
        synchronized (this) {
            objs.addAll(this.enemies);
            objs.addAll(this.collisions);
        }
        objs.addAll(this.missiles);
        objs.add(this.cannon);
        objs.add(this.modelInfo);
        return objs;
    }
    
    public void timeTick(){
        this.processCommands();
        this.moveMissiles();
        
    }
    
    private void moveMissiles(){
        for(AbsMissile m : this.missiles) {
            ArrayList <AbsEnemy> enemiesToRemove = this.enemiesHitByMissile(m);
            for (AbsEnemy e : enemiesToRemove){
                this.enemies.remove(e);
                synchronized(this) {
                    this.score += 1;
                }
            }
            m.move();
        }
        
        for(ObstacleComponent obstacle : this.obstacles) {
            if(obstacle.shouldRemoveAfterHit(this.missiles)){
                this.obstacles.remove(obstacle);
            }
        }
        
        
        synchronized(this) {
            for(AbsMissile m : this.missiles) {
                if(m.getX() > MvcGameConfig.MAX_X || m.getY() > MvcGameConfig.MAX_Y) {
                    this.missiles.remove(m);
                }
            }
        }

        this.notifyObservers();
    }
    
    private ArrayList<AbsEnemy> enemiesHitByMissile(AbsMissile m) {
        ArrayList<AbsEnemy> enemiesToRemove = new ArrayList<>();
        for (AbsEnemy e : this.enemies) {
            if (Math.abs(m.getX() - e.getX()) < MvcGameConfig.MAX_MISS_OF_ENEMY
                    && Math.abs(m.getY() - e.getY()) < MvcGameConfig.MAX_MISS_OF_ENEMY) {
                AbsCollision collission = this.goFact.createCollision();
                collission.setPositionIfNull(new Position(e.getX(), e.getY()));
                this.collisions.add(collission);
                enemiesToRemove.add(e);
            }
        }
        return enemiesToRemove;
    }
    

    @Override
    public void registerObserver(IObserver obs) {
        this.myObservers.add(obs);
    }

    @Override
    public void unregisterObserver(IObserver obs) {
        this.myObservers.remove(obs);
    }

    @Override
    public void notifyObservers() {
        for (IObserver obs : this.myObservers) {
            obs.update();
        }
    }

    @Override
    public void cannonShoot() {
        synchronized(this) {
            this.missiles.addAll(this.cannon.shoot());
        }
        this.notifyObservers();
    }

    public String getCannonShootingModeName(){
        return this.cannon.actualMode().getName();
    }
    
    @Override
    public void cannonToggleMode() {
        this.cannon.toggle();
        this.notifyObservers();
    }

    @Override
    public void registerCommand(ICommand command) {
        this.unexecutedCommands.add(command);
    }

    private void processCommands() {
        while(!this.unexecutedCommands.isEmpty()){
            ICommand command = this.unexecutedCommands.poll();
            command.doExecute(this.context, true);
            this.executedCommands.push(command);
        }
    }

    @Override
    public void setMemento(Object memento) {
       Memento m = (Memento) memento;
       this.score = m.score;
       int differenceX = m.cannonX - this.cannon.getX();
       int differenceY = m.cannonY - this.cannon.getY();
       this.cannon.move(differenceX, differenceY);
       this.cannon.setAngle(m.cannonAngle);
       this.cannon.setForce(m.cannonForce);
       this.missiles.clear();
       this.enemies.clear();
        for(Position position : m.missilePositions){
            AbsMissile missile = this.goFact.createMissile();
            missile.setPositionIfNull(position);
            this.missiles.add(missile);
        }
        
        for(Position position : m.enemyPositions){
            AbsEnemy enemy = this.goFact.createEnemy();
            enemy.setPositionIfNull(position);
            this.enemies.add(enemy);
        }
        this.notifyObservers();
    }

    @Override
    public void undoLastCommand() {
        if (!this.executedCommands.empty()) {
            ICommand command = this.executedCommands.pop();
            command.unexecute(this.context);

            this.notifyObservers();
        }
    }

    @Override
    public void cannonChangeAngle(boolean increase) {
        if (increase) {
            this.cannon.increaseAngle();
        } else {
            this.cannon.decreaseAngle();
        }
    }

    @Override
    public void cannonChangeForce(boolean increase) {
        if (increase) {
            this.cannon.increaseForce();
        } else {
            this.cannon.decreaseForce();
        }
    }

    @Override
    public int getScore() {
        return this.score;
    }

    @Override
    public int getLevel() {
        return this.level;
    }
    
    private class Memento {
        private int score;
        private int cannonX;
        private int cannonY;
        private int cannonAngle;
        private int cannonForce;
        private List<Position> missilePositions;
        private List<Position> enemyPositions;
        
        // další atributy pro uložení - pozice nepřátel, kulek, kanónu
    }
    
    public Object createMemento(){
        Memento m = new Memento();
        m.score = this.score;
        m.cannonX = this.cannon.getX();
        m.cannonY = this.cannon.getY();
        m.cannonAngle = this.cannon.getAngle();
        m.cannonForce = this.cannon.getForce();
        m.missilePositions = new ArrayList<Position>();
        m.enemyPositions = new ArrayList<Position>();
        for(AbsMissile missile : this.missiles){
            m.missilePositions.add(new Position(missile.getX(), missile.getY()));
        }
        for(AbsEnemy enemy : this.enemies){
            m.enemyPositions.add(new Position(enemy.getX(), enemy.getY()));
        }
        return m;
    }
    
    public void restoreMemento(Object memento){
        Memento m = (Memento) memento;
        this.score = m.score;
        int differenceX = this.cannon.getX() - m.cannonX;
        int differenceY = this.cannon.getY() - m.cannonY;
        this.cannon.move(differenceX, differenceY);
        this.cannon.setAngle(m.cannonAngle);
        this.cannon.setForce(m.cannonForce);
        this.missiles.clear();
        this.enemies.clear();
        for(Position position : m.missilePositions){
            AbsMissile missile = this.goFact.createMissile();
            missile.setPositionIfNull(position);
            synchronized(this) {
                this.missiles.add(missile);
            }
        }
        for(Position position : m.enemyPositions){
            AbsEnemy enemy = this.goFact.createEnemy();
            enemy.setPositionIfNull(position);
            this.enemies.add(enemy);
        }
        this.notifyObservers();
    }
     
    
    public int getCannonAngle(){
        return this.cannon.getAngle();
    }
    
    public int getCannonForce(){
        return this.cannon.getForce();
    }
    
    public List<ObstacleComponent> getObstacles(){
        return this.obstacles;
    }
    
    public boolean won(){
        return this.enemies.isEmpty();
    }
}
