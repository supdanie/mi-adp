/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.abstractFactory;

import cz.cvut.fit.miadp.mvcgame.composite.CompositeObstacle;
import cz.cvut.fit.miadp.mvcgame.composite.ObstacleComponent;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCannon;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsCollision;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsEnemy;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsMissile;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.AbsModelInfo;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.RectangleWall;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.Cannon_A;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyB.Cannon_B;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyB.Collision_B;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyB.Enemy_B;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyB.Missile_B;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyB.ModelInfo_B;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMovingStrategy;

/**
 *
 * @author user
 */
public class GameObjsFact_B implements IGameObjsFact {

    @Override
    public AbsCannon createCannon() {
        Cannon_B c = new Cannon_B();
        Position cannonPos = new Position(MvcGameConfig.CANNON_INIT_X, MvcGameConfig.CANNON_INIT_Y);
        c.setPositionIfNull(cannonPos);
        return c;
    }  

    @Override
    public AbsEnemy createEnemy() {
        return new Enemy_B();
    }

    @Override
    public AbsCollision createCollision() {
        return new Collision_B();
    }

    @Override
    public AbsMissile createMissile() {
        return new Missile_B(new SimpleMovingStrategy());
    }

    @Override
    public AbsModelInfo createModelInfo() {
        return new ModelInfo_B();
    }
    
     @Override
    public ObstacleComponent createSimpleObstacle(int startX, int startY, boolean vertical) {
        CompositeObstacle component = new CompositeObstacle();
        for (int i = 0; i < 5; i++) {
            RectangleWall wall = new RectangleWall();
            int actualX = vertical ? startX : startX + i * MvcGameConfig.OBSTACLE_SIZE;
            int actualY = vertical ? startY + i * MvcGameConfig.OBSTACLE_SIZE : startX;
            wall.setPositionIfNull(new Position(actualX, actualY));
            component.add(wall);
        }
        return component;
    }
}
