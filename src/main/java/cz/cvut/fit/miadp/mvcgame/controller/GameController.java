/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.controller;

import cz.cvut.fit.miadp.mvcgame.command.CannonChangeAngleCommand;
import cz.cvut.fit.miadp.mvcgame.command.CannonChangeForceCommand;
import cz.cvut.fit.miadp.mvcgame.command.CannonMoveDownCommand;
import cz.cvut.fit.miadp.mvcgame.command.CannonMoveUpCommand;
import cz.cvut.fit.miadp.mvcgame.command.CannonShootCommand;
import cz.cvut.fit.miadp.mvcgame.command.CannonToggleModeCommand;
import cz.cvut.fit.miadp.mvcgame.interpreter.CommandSequence;
import cz.cvut.fit.miadp.mvcgame.interpreter.CommandSequenceBuilder;
import cz.cvut.fit.miadp.mvcgame.interpreter.ICommand;
import cz.cvut.fit.miadp.mvcgame.interpreter.RepeatedCommand;
import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;

/**
 *
 * @author user
 */
public class GameController {
    private IGameModel model;
    
    public GameController(IGameModel model){
        this.model = model;
    }
    
    public void handleKeyPress(String code){
        switch(code) {
            case "UP":
                this.model.registerCommand(new CannonMoveUpCommand(this.model));
                break;
            case "DOWN":
                this.model.registerCommand(new CannonMoveDownCommand(this.model));
                break;
            case "SPACE":
                this.model.registerCommand(new CannonShootCommand(this.model));
                break;
            case "M":
                this.model.registerCommand(new CannonToggleModeCommand(this.model));
                break;
            case "LEFT":
                this.model.registerCommand(new CannonChangeAngleCommand(this.model, true));
                break;
            case "RIGHT":
                this.model.registerCommand(new CannonChangeAngleCommand(this.model, false));
                break;
            case "W":
                this.model.registerCommand(new CannonChangeForceCommand(this.model, true));
                break;
            case "S":
                this.model.registerCommand(new CannonChangeForceCommand(this.model, false));
                break;
            case "T":
                CommandSequenceBuilder builder = new CommandSequenceBuilder(this.model);
                builder.addCommand(new RepeatedCommand(this.model, 7, new CannonMoveDownCommand(this.model)));
                builder.addCommand(new CannonShootCommand(this.model));
                CommandSequence downSequence = builder.build();    
                ICommand repeatedDownSequence = new RepeatedCommand(this.model, 4, downSequence);
                builder.clearCommands();
                builder.addCommand(new RepeatedCommand(this.model, 7, new CannonMoveUpCommand(this.model)));
                builder.addCommand(new CannonShootCommand(this.model));
                CommandSequence upSequence = builder.build();
                ICommand repeatedUpSequence = new RepeatedCommand(this.model, 4, upSequence);
                builder.clearCommands();
                builder.addCommand(repeatedDownSequence);
                builder.addCommand(repeatedUpSequence);
                ICommand shootingFire = builder.build();
                
                this.model.registerCommand(shootingFire);
                break;
            case "Z":
                this.model.undoLastCommand();
                break;
            default:
                //nothing
        }
    }
}
