/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.bridge;

import cz.cvut.fit.miadp.mvcgame.model.Position;

/**
 *
 * @author user
 */
public interface IGameGraphicsImplementor {
    public void drawImage(String path, Position pos);
    public void drawText(String text, Position pos);
    public void drawLine(Position startPos, Position endPos);

    public void clearRect(int i, int i0, int MAX_X, int MAX_Y);
}
