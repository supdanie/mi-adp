/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.interpreter;

import java.util.HashMap;

/**
 *
 * @author user
 */
public class MacrocommandContext {
    private HashMap<Integer, Object> mementos;
    
    public MacrocommandContext(){
        this.mementos = new HashMap<>();
    }
    
    public void setMementoForCommand(int commandID, Object memento){
        this.mementos.put(commandID, memento);
    }
    
    public Object getMementoForCommand(int commandID){
        return this.mementos.get(commandID);
    }
}
