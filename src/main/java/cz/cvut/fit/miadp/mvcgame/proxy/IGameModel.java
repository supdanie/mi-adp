/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.cvut.fit.miadp.mvcgame.proxy;

import cz.cvut.fit.miadp.mvcgame.command.AbstractCommand;
import cz.cvut.fit.miadp.mvcgame.composite.ObstacleComponent;
import cz.cvut.fit.miadp.mvcgame.interpreter.ICommand;
import java.util.List;

/**
 *
 * @author user
 */
public interface IGameModel {
    public void setGravity(int gravity);
    public int getGravity();
    public String getCannonShootingModeName();
    public int getCannonAngle();
    public int getCannonForce();
    public int getScore();
    public int getLevel();
    public void moveCannonUp();
    public void moveCannonDown();
    public void cannonShoot();
    public void cannonToggleMode();
    public void cannonChangeAngle(boolean increase);
    public void cannonChangeForce(boolean increase);
    public void registerCommand(ICommand command);
    
    public Object createMemento();
    public void setMemento(Object memento);
    public void undoLastCommand();
    
    public List<ObstacleComponent> getObstacles();
}
