
import cz.cvut.fit.miadp.mvcgame.abstractFactory.GameObjsFact_B;
import cz.cvut.fit.miadp.mvcgame.abstractFactory.IGameObjsFact;
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.Cannon_A;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyB.Missile_B;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMovingStrategy;
import org.junit.Test;
import org.mockito.Mockito;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
public class TestCannon {
    @Test
    public void testShoot(){
        IGameObjsFact factoryMock = mock(IGameObjsFact.class);
        Mockito.when(factoryMock.createMissile()).thenReturn(new Missile_B(new SimpleMovingStrategy()));
        
        Cannon_A cannon = new Cannon_A(factoryMock);
        cannon.setPositionIfNull(new Position(MvcGameConfig.CANNON_INIT_X, MvcGameConfig.CANNON_INIT_Y));
        cannon.shoot();
        verify(factoryMock, atLeastOnce()).createMissile();
        cannon.useDoubleMode();
        cannon.shoot();
        verify(factoryMock, times(3)).createMissile();
    }
}
