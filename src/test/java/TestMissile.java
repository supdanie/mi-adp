
import cz.cvut.fit.miadp.mvcgame.config.MvcGameConfig;
import cz.cvut.fit.miadp.mvcgame.model.Position;
import cz.cvut.fit.miadp.mvcgame.model.gameObjects.familyA.Missile_A;
import cz.cvut.fit.miadp.mvcgame.strategy.RealisticMovingStrategy;
import cz.cvut.fit.miadp.mvcgame.strategy.SimpleMovingStrategy;
import static org.junit.Assert.assertTrue;
import org.junit.Test;
import static org.mockito.Mockito.mock;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
public class TestMissile {
    @Test
    public void testMissileWithSimpleStrategyAndZeroAngle(){
        SimpleMovingStrategy strategy = new SimpleMovingStrategy();
        strategy.setGravity(5);
        Missile_A missile = new Missile_A(strategy);
        missile.setPositionIfNull(new Position(MvcGameConfig.CANNON_INIT_X, 1000));
        missile.setInitialAngle(0);
        missile.setInitialForce(15);
        int startX = missile.getX(); 
        int startY = missile.getY();
        missile.move();
        int endX = missile.getX();
        int endY = missile.getY();
        assertTrue(endX - startX == missile.getInitialForce());
        assertTrue(endY == startY);
    }
    
    @Test
    public void testMissileWith45DegreesAngleAndSimpleStrategy(){
        SimpleMovingStrategy strategy = new SimpleMovingStrategy();
        strategy.setGravity(5);
        Missile_A missile = new Missile_A(strategy);
        missile.setPositionIfNull(new Position(MvcGameConfig.CANNON_INIT_X, 1000));
        missile.setInitialAngle(45);
        missile.setInitialForce(15);
        int startX = missile.getX(); 
        int startY = missile.getY();
        missile.move();
        int endX = missile.getX();
        int endY = missile.getY();
        assertTrue(endX - startX == missile.getInitialForce());
        assertTrue(endY - startY == -missile.getInitialForce());
    }
    
    @Test
    public void testMissileWithMinus45DegreesAngleAndSimpleStrategy(){
        SimpleMovingStrategy strategy = new SimpleMovingStrategy();
        strategy.setGravity(5);
        Missile_A missile = new Missile_A(strategy);
        missile.setPositionIfNull(new Position(MvcGameConfig.CANNON_INIT_X, 1000));
        missile.setInitialAngle(-45);
        missile.setInitialForce(15);
        int startX = missile.getX(); 
        int startY = missile.getY();
        missile.move();
        int endX = missile.getX();
        int endY = missile.getY();
        assertTrue(endX - startX == missile.getInitialForce());
        assertTrue(endY - startY == missile.getInitialForce());
    }
    
    @Test
    public void testMissileWith45DegreesAndRealisticStrategy(){
        RealisticMovingStrategy strategy = new RealisticMovingStrategy();
        strategy.setGravity(5);
        Missile_A missile = new Missile_A(strategy);
        missile.setPositionIfNull(new Position(MvcGameConfig.CANNON_INIT_X, 1000));
        missile.setInitialAngle(45);
        missile.setInitialForce(15);
        int startX = missile.getX(); 
        missile.move();
        int endX = missile.getX();
        int endY = missile.getY();
        
        int diffX = endX - MvcGameConfig.CANNON_INIT_X;
        int correct = 1000 - (int)(Math.tan(Math.toRadians(45)) * diffX) + (int) (0.5 * diffX * diffX / 225);
        assertTrue(endX - startX == missile.getInitialForce());
        assertTrue(endY == correct);
    }
}
