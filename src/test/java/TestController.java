
import cz.cvut.fit.miadp.mvcgame.command.CannonChangeAngleCommand;
import cz.cvut.fit.miadp.mvcgame.command.CannonChangeForceCommand;
import cz.cvut.fit.miadp.mvcgame.command.CannonMoveDownCommand;
import cz.cvut.fit.miadp.mvcgame.command.CannonMoveUpCommand;
import cz.cvut.fit.miadp.mvcgame.command.CannonShootCommand;
import cz.cvut.fit.miadp.mvcgame.controller.GameController;
import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;
import org.junit.Test;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
public class TestController {
    @Test
    public void testHandleKeyCodes(){
        IGameModel modelMock = mock(IGameModel.class);
        GameController controller = new GameController(modelMock);
        controller.handleKeyPress("UP");
        verify(modelMock, times(1)).registerCommand(new CannonMoveUpCommand(modelMock));
        controller.handleKeyPress("DOWN");
        verify(modelMock, times(1)).registerCommand(new CannonMoveDownCommand(modelMock));
        controller.handleKeyPress("SPACE");
        verify(modelMock, times(1)).registerCommand(new CannonShootCommand(modelMock));
        controller.handleKeyPress("LEFT");
        verify(modelMock, times(1)).registerCommand(new CannonChangeAngleCommand(modelMock, true));
        controller.handleKeyPress("RIGHT");
        verify(modelMock, times(1)).registerCommand(new CannonChangeAngleCommand(modelMock, false));
        controller.handleKeyPress("W");
        verify(modelMock, times(1)).registerCommand(new CannonChangeForceCommand(modelMock, true));
        controller.handleKeyPress("S");
        verify(modelMock, times(1)).registerCommand(new CannonChangeForceCommand(modelMock, false));
        controller.handleKeyPress("Z");
        verify(modelMock, times(1)).undoLastCommand();
    }
}
