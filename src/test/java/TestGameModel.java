
import cz.cvut.fit.miadp.mvcgame.command.CannonMoveDownCommand;
import cz.cvut.fit.miadp.mvcgame.model.GameModel;
import cz.cvut.fit.miadp.mvcgame.observer.IObserver;
import java.util.ArrayList;
import org.junit.Test;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
public class TestGameModel {
    @Test
    public void testObserverMoveCannonUp(){
        GameModel model = new GameModel(new ArrayList<>(), new ArrayList<>());
        IObserver observerMock = mock(IObserver.class);
        model.registerObserver(observerMock);
        model.moveCannonUp();
        verify(observerMock, atLeastOnce()).update();
    }
    
    @Test
    public void testObserverCannonShoot(){
        GameModel model = new GameModel(new ArrayList<>(), new ArrayList<>());
        IObserver observerMock = mock(IObserver.class);
        model.registerObserver(observerMock);
        model.cannonShoot();
        verify(observerMock, atLeastOnce()).update();
    }
    
    @Test
    public void testObserverWithCommand(){
        GameModel model = new GameModel(new ArrayList<>(), new ArrayList<>());
        IObserver observerMock = mock(IObserver.class);
        model.registerObserver(observerMock);
        model.registerCommand(new CannonMoveDownCommand(model));
        model.timeTick();
        verify(observerMock, atLeastOnce()).update();
    }
}
