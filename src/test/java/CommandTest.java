
import cz.cvut.fit.miadp.mvcgame.command.CannonMoveDownCommand;
import cz.cvut.fit.miadp.mvcgame.command.CannonMoveUpCommand;
import cz.cvut.fit.miadp.mvcgame.command.CannonShootCommand;
import cz.cvut.fit.miadp.mvcgame.command.CannonToggleModeCommand;
import cz.cvut.fit.miadp.mvcgame.proxy.IGameModel;
import org.junit.Test;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author user
 */
public class CommandTest {
    @Test
    public void testCannonMoveUpCommand(){
        IGameModel receiverMock = mock(IGameModel.class);
        CannonMoveUpCommand command = new CannonMoveUpCommand(receiverMock);
        command.execute();
        verify(receiverMock, atLeastOnce()).moveCannonUp();
    }
    
    @Test
    public void testCannonMoveDownCommand(){
        IGameModel receiverMock = mock(IGameModel.class);
        CannonMoveDownCommand command = new CannonMoveDownCommand(receiverMock);
        command.execute();
        verify(receiverMock, times(1)).moveCannonDown();
    }
    
    @Test
    public void testCannonShootCommand(){
        IGameModel receiverMock = mock(IGameModel.class);
        CannonShootCommand command = new CannonShootCommand(receiverMock);
        command.execute();
        verify(receiverMock, times(1)).cannonShoot();
    }
    
    @Test
    public void testCannonToggleModeCommand(){
        IGameModel receiverMock = mock(IGameModel.class);
        CannonToggleModeCommand command = new CannonToggleModeCommand(receiverMock);
        command.execute();
        verify(receiverMock, times(1)).cannonToggleMode();
    }
}
